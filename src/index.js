(() => {
  // fake-query.js
  addEventListener("fetch", (event) => {
    event.respondWith(handle(event.request));
  });
  var xataApiKey = XATA_KEY;
  var corsHeaders = {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET, HEAD, POST, OPTIONS",
    "Access-Control-Allow-Headers": "*",
    "Content-Type": "application/json"
  };
  async function handle(request) {
    if (request.method === "OPTIONS") {
      return handleOptions(request);
    } else if (request.method === "POST") {
      return handlePost(request);
    } else {
      return new Response(null, {
        status: 405,
        statusText: "Method Not Allowed"
      });
    }
  }
  function handleOptions(request) {
    if (request.headers.get("Origin") !== null && request.headers.get("Access-Control-Request-Method") !== null && request.headers.get("Access-Control-Request-Headers") !== null) {
      return new Response(null, {
        headers: corsHeaders
      });
    } else {
      return new Response(null, {
        headers: {
          "Allow": "GET, HEAD, POST, OPTIONS"
        }
      });
    }
  }
  async function handlePost(request) {
    let json = await request.json();
    let filter = {};
    let sortFilter = {};

    let category = "Samples";
    let search = json.search.toLowerCase();
    let group = json.group;
    let type = json.type;
    let sort = json.sort;
    let pageSize = 20;
    let pageOffset = Number(json.page) * pageSize;

    if (group === "\u2736" || group === "") {
      filter = {"search": {"$contains": search}};
    } else if (type === "\u2736" || type === "") {
      filter = {"search": {"$contains": search}, "group": group};
    } else {
      filter = {"search": {"$contains": search}, "group": group, "type": type };
    };

    if(sort === "A-Z"){
      sortFilter = {"name": "asc"};
    } else if (sort === "Z-A"){
      sortFilter = {"name": "desc"};
    } else if (sort === "NEW"){
      sortFilter = {"xata.createdAt": "desc"};
    } else if (sort === "OLD"){
      sortFilter = {"xata.createdAt": "asc"};
    } else {
      sortFilter = {"*": "random"};
    }

    let pageQuery = {"size": pageSize, "offset": pageOffset};

    //sort stuff: asc-ascending desc-descending
    let options = {
      method: "POST",
      headers: { Authorization: `Bearer ${xataApiKey}`, "Content-Type": "application/json" },
      body: `{
            "filter": ${JSON.stringify(filter)},
            "sort": ${JSON.stringify(sortFilter)},
            "page": ${JSON.stringify(pageQuery)}
        }`
    };
    const data = await fetch(`https://fake-data-q82dso.us-west-2.xata.sh/db/fake-data:main/tables/${category}/query`, options);
    const query = await data.json();
    return new Response(JSON.stringify(query), { headers: corsHeaders });
  }
})();
//# sourceMappingURL=fake-query.js.map
